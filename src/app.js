import React, { useState } from 'react';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import FirstScreen from './components/FirstScreen/FirstScreen';
import SecondScreen from './components/SecondScreen/SecondScreen';
import './app.css';

const soundBoolean = config.settings.soundPlayed;

const Main = Styled.main`
  font-family: ${config.settings.fontName}, serif;
  background-image: url(${config.settings.bgImg});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: 0 0;
`;

const App = () => {
  const [secondDisplay, setSecondDisplay] = useState(false);
  const [clicks, setClicks] = useState(1);
  const [soundOn, setSoundOn] = useState(soundBoolean);
  console.log(config.settings.font);

  const clickCheckerHandler = () => {
    if (clicks === config.settings.clicks) {
      setSecondDisplay(true);
    }
  };

  const clickCounterHandler = () => {
    setClicks(clicks + 1);
    clickCheckerHandler();
  };

  const revertHandler = () => {
    setClicks(1);
    setSecondDisplay(false);
  };

  const soundHandler = () => {
    setSoundOn(!soundOn);
  };

  return (
    <Main className="main">
      {secondDisplay ? (
        <SecondScreen back={revertHandler} soundOn={soundOn} />
      ) : (
        <FirstScreen
          clicks={clicks}
          clickCounter={clickCounterHandler}
          soundOn={soundOn}
          soundHandler={soundHandler}
        />
      )}
    </Main>
  );
};

export default App;
